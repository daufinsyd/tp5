//
// Created by sydney_manjaro on 18/10/16.
//

#include "VecteurReel.h"
#include <vector>
#include <iostream>

vecteurReel::vecteurReel(int size) {
    try {
        if (size < 1) {
            throw size;
        }
        else {
            if (size < 10) {
                size *= 2;
            }
            std::vector<double> array(size);
            this->array = array;
            capa = size;
            size = 0;
        }
    }
    catch (int e) { std::cout << "Cannot create a vector of size: " << e << std::endl;}
}

vecteurReel::vecteurReel(vecteurReel &v) {
    std::vector<double> array(v.size());
    array = array;
    capa = v.capacity();
    size_m = v.size();

    std::cout << "Create copy " << std::endl;
}

vecteurReel::~vecteurReel() {
    std::cout << "Vector deleted" << std::endl;
}

vecteurReel& vecteurReel::operator=(vecteurReel &v) {
    if (this != &v) {
        std::vector<double> array;
        array.swap(v.array);
        size_m = v.size_m;
        capa = v.capa;
    }
}

const int vecteurReel::capacity() {
    return capa;
}

const int vecteurReel::size() {
    return size_m;
}

const std::vector<double> vecteurReel::getArray(){
    return this->array;
}