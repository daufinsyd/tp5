//
// Created by sydney_manjaro on 18/10/16.
//

#ifndef T5_VECTEURREEL_H
#define T5_VECTEURREEL_H

#include <vector>

class vecteurReel {
public:
    vecteurReel(int=10);
    vecteurReel(vecteurReel&);
    ~vecteurReel();

    vecteurReel& operator=(vecteurReel&);

    const int size();
    const int capacity();
    const std::vector<double> getArray();

private:
    int capa;
    int size_m;  // taille effectivement utilisee
    std::vector<double> array;

};

#endif //T5_VECTEURREEL_H
